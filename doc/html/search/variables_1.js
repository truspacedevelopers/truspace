var searchData=
[
  ['data_0',['data',['../classtensor2D.html#adc82d196f1d710ae6762e4ea1720f5e3',1,'tensor2D::data'],['../classtensor3D.html#a0275b6f4c55df4b4ef95d0a933777cc2',1,'tensor3D::data'],['../classtensor4D.html#ac0a2dd341e0de829bba5b0198e0c970f',1,'tensor4D::data'],['../classtensor5D.html#a738d56120b89b360f5d81fd61472b7de',1,'tensor5D::data'],['../classtensor6D.html#a9a5d2710f7acd9e7a4b3fe1cce185804',1,'tensor6D::data']]],
  ['deltas_1',['deltas',['../classminimal__model__parameters.html#a0b97dd692a43f1d6b9968d05f5c137de',1,'minimal_model_parameters']]],
  ['desch_2',['descH',['../classHamiltonian.html#ac4be6eeb4ab7f225b32c7633bc883541',1,'Hamiltonian']]]
];
