var indexSectionsWithContent =
{
  0: "abcdefghiklmnopqrstvw~",
  1: "cdfhkmnpstw",
  2: "bcdefghmnpstw",
  3: "abcdefghikmnoprstw~",
  4: "cdefhiklmnopqrstv",
  5: "m",
  6: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "defines",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Macros",
  6: "Pages"
};

