ROOT=${PWD}/../..
CLN=${ROOT}/dep/cln-1.3.7
GINAC=${ROOT}/dep/ginac-1.8.7
SCALAPACK_LIB= -lscalapack
NRG_LIB= -L${ROOT}/lib/ -lnrg
NRG_LIB_MPI=  -L${ROOT}/lib -lnrg_mpi
CXX= icc
MPICXX= mpicxx

CFLAGS=-I${CLN}/include -I${GINAC}/include -I${ROOT}/include -std=c++20 -O2  -DMKL_ILP64  -I"${MKLROOT}/include" -DMKL 
LIBS= -L${GINAC}/lib/ -lginac -L${CLN}/lib/ -lcln 
LDFLAGS =  -L${MKLROOT}/lib -Wl,-rpath,${MKLROOT}/lib -lmkl_intel_ilp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl
LDFLAGS_MPI = -L${MKLROOT}/../../compiler/latest/mac/compiler/lib  -L${MKLROOT}/lib -Wl,-rpath ${MKLROOT}/../../compiler/latest/mac/compiler/lib -liomp5 -Wl,-rpath ${MKLROOT}/lib -lmkl_scalapack_ilp64 -lmkl_intel_ilp64 -lmkl_intel_thread -lmkl_core -lmkl_blacs_mpich_ilp64 -lpthread -lm -ldl
SRC=characters.cpp gamma.cpp cpt.cpp block.cpp worker.cpp element_computation.cpp minimal_model_states.cpp diag.cpp minimal_model_filenames.cpp chirME.cpp minimal_model_parameters.cpp
SRC_FIELD= gamma.cpp cpt.cpp

OBJS=$(SRC:.cpp=.o)
#OBJS_MPI=$(SRC_MPI:.cpp=.o)
OBJS_FIELD=$(SRC_FIELD:.cpp=.o)

.PHONY: clean test test_mpi

serial: truspace.x  fields_and_structure_constants.x

mpi : truspace_mpi.x fields_and_structure_constants.x

$(OBJS): %.o : %.cpp %.h minimal_model_config.h
	$(CXX) $(CFLAGS) -c $<

minimal_model_Hamiltonian.o: minimal_model_Hamiltonian.cpp minimal_model_Hamiltonian.h minimal_model_config.h
	$(CXX) ${CFLAGS} -c $<

minimal_model_Hamiltonian_MPI.o: minimal_model_Hamiltonian_MPI.cpp minimal_model_Hamiltonian_MPI.h minimal_model_config.h
	$(CXX) ${CFLAGS} -c $<

truspace.x : CFLAGS += -fopenmp
truspace.x : truspace.cpp $(OBJS) minimal_model_Hamiltonian.o
	${CXX} ${CFLAGS}  $^ ${LDFLAGS} ${NRG_LIB} ${LIBS}  -o  $@  -Wl,-rpath ${CLN}/lib/ -Wl,-rpath ${GINAC}/lib/ -Wl,-rpath ${ROOT}/lib/
	mkdir -p ${ROOT}/bin
	cp $@ ${ROOT}/bin/

truspace_mpi.x : CFLAGS += -DMPI_PAR
truspace_mpi.x : CXX=${MPICXX}
truspace_mpi.x : truspace.cpp $(OBJS) minimal_model_Hamiltonian_MPI.o 
	${CXX} ${CFLAGS} -m64  ${LIBS} $^ -o  $@  -Wl,-rpath ${CLN}/lib/ -Wl,-rpath ${GINAC}/lib/ -Wl,-rpath ${ROOT}/lib/ ${LDFLAGS_MPI} ${NRG_LIB_MPI} 
	mkdir -p ${ROOT}/bin
	cp $@ ${ROOT}/bin

fields_and_structure_constants.x : fields_and_structure_constants.cpp $(OBJS_FIELD) 
	$(CXX)  -I${ROOT}/include -std=c++11 -O2  $^ -o $@
	mkdir -p ${ROOT}/bin
	cp $@ ${ROOT}/bin/        

clean:
	rm -f *.x *.o

test:
	@rm ${ROOT}/test/Eigenvalues_p_3_q_4_minLev_0_maxLev_10_Per_2_c_0.09_Subspace_0_Mom_0 || true
	@rm ${ROOT}/test/FINAL_Eigenvalues_p_3_q_4_minLev_0_maxLev_10_Per_2_c_0.09_Subspace_0_Mom_0 || true
	@rm ${ROOT}/test/store* || true
	@cp truspace.x ${ROOT}/test/ && cd ${ROOT}/test/ && ./truspace.x input-ising-model input-ising-nrg > /dev/null
	@python3 -m pytest  --junitxml results.xml  ${ROOT}/test/compare.py

test_mpi:
	@rm ${ROOT}/test/Eigenvalues_p_3_q_4_minLev_0_maxLev_10_Per_2_c_0.09_Subspace_0_Mom_0 || true
	@rm ${ROOT}/test/FINAL_Eigenvalues_p_3_q_4_minLev_0_maxLev_10_Per_2_c_0.09_Subspace_0_Mom_0 || true
	@rm ${ROOT}/test/store* || true
	@cp truspace_mpi.x ${ROOT}/test/ && cd ${ROOT}/test/ && mpirun -np 4 ./truspace_mpi.x input-ising-model input-ising-nrg > /dev/null
	@python3 -m pytest  --junitxml results.xml  ${ROOT}/test/compare_mpi.py

